#include "cuda_fp16.h"

__global__
void add(const __half2 *x, __half2 *y) {
    uint off = blockIdx.x;
    y[off] = __hadd2(x[off * 2 + 0], x[off * 2 + 1]);
}
