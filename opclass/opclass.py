#!/usr/bin/env python3
import subprocess, struct, tempfile
import re
import concurrent.futures
import itertools

does_not_exist = b"nvdisasm error   : Unrecognized operation for functional unit 'uC' at address 0x00000000\n"
opclass_re = re.compile(r"nvdisasm error   : Opclass \'([^\']*)\'")

def check_op(i):
    (op, bits90_91) = i
    with tempfile.TemporaryDirectory() as d:
        file_path = d + f"/{op}_{bits90_91}"
        #print('__________________')
        #print(i + 64)
        #print("{:04b}".format(i), flush=True)
        #print("{:03x}".format(i), flush=True)

        vals = [0x0000000000007000, 0x000ffe0008000000]
        vals[0] += op
        vals[1] += bits90_91 << 26

        with open(file_path, 'wb') as f:
            for v in vals:
                f.write(struct.pack('<Q', v))

        proc = subprocess.run(["nvdisasm", "-c", "--binary", "SM75", file_path], capture_output=True)
        if proc.stderr == does_not_exist:
            # print("None here!")
            return None
        else:
            found_match = opclass_re.match(proc.stderr.decode('utf-8'))
            # print(found_match)
            if found_match is None:
                msg = proc.stderr
            else:
                msg = found_match.group(1)
            return "0x{:03x} 0x{:x} {}".format(op, bits90_91, msg)


with concurrent.futures.ThreadPoolExecutor() as executor:
    for res in executor.map(check_op, itertools.product(range(0xfff), range(4))):
        if res is not None:
            print(res)
