// nvcc deeply_nested.cu --cubin -gencode arch=compute_60,code=sm_80 && nvdisasm -hex deeply_nested.cubin > deeply_nested_disasm

template<unsigned int i>
__device__
void nested_branch(const unsigned int *in, unsigned int *out) {
    if (threadIdx.x & in[i]) {
        nested_branch<i - 1>(in, out);
    }
    out[threadIdx.x] = in[threadIdx.x];
}

template<>
__device__
void nested_branch<0>(const unsigned int *in, unsigned int *out) {
    out[threadIdx.z] = in[threadIdx.y];
}

__global__ void kernel(const unsigned int *in, unsigned int *out) {
    nested_branch<50>(in, out);
}

int main(){}
