#!/usr/bin/env python3
import subprocess, struct, tempfile


with tempfile.TemporaryDirectory() as d:
    for i in range(0, 1 << 4):

        file_path = d + f"/{i}"

        print('__________________')
        print("{:04b}".format(i), flush=True)
        print("{:x}".format(i), flush=True)

        vals = [0x000000ff02007986, 0x000fe2000c101904]
        #vals = [0x000000ffff007986, 0x000fe2000c10e904]
        vals[1] &= ~(((1 << 4) - 1) << 13)
        vals[1] |= i << 13

        with open(file_path, 'wb') as f:
            for v in vals:
                f.write(struct.pack('<Q', v))

        subprocess.run(["nvdisasm", "-c", "--binary", "SM80", file_path])
