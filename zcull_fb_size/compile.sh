#!/usr/bin/bash
set -e

rm -rf cache
glslangValidator -V 27_shader_depth.vert -o vert.spv
glslangValidator -V 27_shader_depth.frag -o frag.spv
g++ 27_depth_buffering.cpp -lvulkan -o main
