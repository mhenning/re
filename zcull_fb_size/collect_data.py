#!/usr/bin/env python3

ehks_lib = "/home/m/comp/mesa/envyhooks/target/debug/libenvyhooks.so"
nv_push_dump = "/home.ext4/m/comp/mesa/mesa/build-rel/src/nouveau/headers/nv_push_dump"
arch = "AMPERE"

import csv, dataclasses, glob, os, re, subprocess, tempfile
from dataclasses import dataclass
from typing import List, Optional
import concurrent.futures
import itertools


def str_re(name):
    return re.compile(r"\s+." + name + r" = ([_A-Za-z0-9]*)")


def hex_re(name):
    return re.compile(r"\s+." + name + r" = \((0x[0-9a-fA-F]+)\)")


TYPE_RE = str_re("TYPE")
NORMALIZED_ALIQUOTS_RE = hex_re("NORMALIZED_ALIQUOTS")
START_ALIQUOT_RE = hex_re("START_ALIQUOT")
ALIQUOT_COUNT_RE = hex_re("ALIQUOT_COUNT")
V_RE = hex_re("V")


def hex_field(r, val):
    match = r.fullmatch(val)
    return int(match.group(1), 0)


def get_bits(x, start_bit_inclusive, end_bit_inclusive):
    assert start_bit_inclusive <= end_bit_inclusive
    count = end_bit_inclusive - start_bit_inclusive + 1
    return (x >> start_bit_inclusive) & ((1 << count) - 1)


@dataclass
class ZcullInfo:
    width: Optional[int] = None
    height: Optional[int] = None
    region_format: Optional[str] = None
    aliquot_count: Optional[int] = None
    normalized_aliquots: Optional[int] = None
    subregion_aliquots: Optional[List[int]] = None
    subregion_format: Optional[List[int]] = None
    subregion_algorithm: Optional[int] = None


def parse_info(dump_str):
    lines = dump_str.splitlines()
    it = iter(lines)
    info = ZcullInfo()
    try:
        while 1:
            line = next(it)
            # print(line)
            if "SET_ZCULL_REGION_FORMAT" in line:
                val = next(it)
                match = TYPE_RE.fullmatch(val)
                info.region_format = match.group(1)
            elif "SET_ZCULL_SUBREGION" in line and not "REPORT_TYPE" in line:
                assert next(it).strip() == ".ENABLE = TRUE"
                info.normalized_aliquots = hex_field(NORMALIZED_ALIQUOTS_RE, next(it))
            elif "SET_ZCULL_REGION_LOCATION" in line:
                start_aliquot = hex_field(START_ALIQUOT_RE, next(it))
                assert start_aliquot == 0
                info.aliquot_count = hex_field(ALIQUOT_COUNT_RE, next(it))
            elif "CALL_MME_MACRO(126)" in line:
                data = [hex_field(V_RE, next(it))]
                while 1:
                    header = next(it)
                    if header == "":
                        break
                    elif "CALL_MME_DATA(126)" in header:
                        data.append(hex_field(V_RE, next(it)))
                    else:
                        raise NotImplementedError()

                assert len(data) == 24
                assert data[0:5] == [0, 0, 0, 2, 1065353216]
                subregion_algorithm = data[5]
                dir_format = data[6]
                assert data[7] == 0
                subregions = data[8:]

                info.subregion_algorithm = subregion_algorithm

                # NVC597_SET_ZCULL_DIR_FORMAT
                zcull_dir = get_bits(dir_format, 0, 15)
                zcull_format = get_bits(dir_format, 16, 31)
                assert zcull_dir == 0
                # print(f"{zcull_format=}")

                info.subregion_aliquots = []
                info.subregion_format = []
                for i, subregion in enumerate(subregions):
                    # NVC597_SET_ZCULL_SUBREGION_ALLOCATION
                    subregion_id = get_bits(subregion, 0, 7)
                    subregion_aliquots = get_bits(subregion, 8, 23)
                    subregion_format = get_bits(subregion, 24, 27)

                    assert i == subregion_id
                    info.subregion_aliquots.append(subregion_aliquots)
                    info.subregion_format.append(subregion_format)
    except StopIteration:
        pass

    return info


def generate_info(i):
    (width, height) = i
    with tempfile.TemporaryDirectory() as tmpdir:
        ehks_env = os.environ.copy()
        ehks_env["LD_PRELOAD"] = ehks_lib
        ehks_env["EHKS_PUSHBUF_DUMP_DIR"] = tmpdir
        subprocess.run(
            ["./main", str(width), str(height)],
            env=ehks_env,
            capture_output=True,
            check=True,
        )

        found_count = 0

        for pushbuf in glob.glob(tmpdir + "/pushbuf_*"):
            dump_str = subprocess.run(
                [nv_push_dump, pushbuf, arch], capture_output=True
            ).stdout.decode("utf-8")

            if not "SET_ZCULL_REGION_FORMAT" in dump_str:
                continue

            found_count += 1
            info = parse_info(dump_str)

        # print(found_count)
        # print(i)
        assert found_count == 1

    # assert info.subregion_aliquots * 16 == info.aliquot_count
    # assert info.subregion_aliquots * 4 == info.normalized_aliquots

    info.width = width
    info.height = height
    return info


def log_range(first, count):
    x = first
    for i in range(count):
        yield x
        x *= 2


list_fields = {"subregion_aliquots": "sal", "subregion_format": "sft"}


def field_names():
    l = list(
        field.name
        for field in dataclasses.fields(ZcullInfo)
        if not field.name in list_fields
    )
    for i in range(16):
        for field in sorted(list_fields.values()):
            l.append(f"{field}{i}")
    return l


def info_to_dict(info):
    x = dataclasses.asdict(info)
    for field in list_fields:
        l = x[field]
        new_name = list_fields[field]
        for i, val in enumerate(l):
            x[f"{new_name}{i}"] = val
        del x[field]
    return x


class Dialect(csv.Dialect):
    def __init__(self):
        self.delimiter = ","
        self.lineterminator = ";\n"
        self.doublequote = False
        self.quotechar = '"'
        self.quoting = csv.QUOTE_NONNUMERIC


if __name__ == "__main__":
    w_multiple = 224
    h_multiple = 32
    widths = log_range(224, 6)  # range(w_multiple, w_multiple * 20, w_multiple)
    heights = log_range(32, 10)  # range(h_multiple, h_multiple * 20, h_multiple)
    with open("data.csv", "w", newline="") as csvfile:
        csvwriter = None
        with concurrent.futures.ThreadPoolExecutor(max_workers=48) as executor:
            for info in executor.map(generate_info, itertools.product(widths, heights)):
                print(info)
                info_dict = info_to_dict(info)
                if csvwriter is None:
                    csvwriter = csv.DictWriter(
                        csvfile, fieldnames=field_names(), dialect=Dialect()
                    )
                    csvwriter.writeheader()
                csvwriter.writerow(info_dict)
