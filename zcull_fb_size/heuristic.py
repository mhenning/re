from hypothesis import given, settings
from hypothesis.strategies import integers
from joblib import Memory

from collect_data import generate_info, ZcullInfo

memory = Memory("./cache", verbose=0)
generate_info_cached = memory.cache(generate_info)


def div_ceil(x, y):
    return (x + y - 1) // y


def next_multiple_of(x, y):
    return div_ceil(x, y) * y


def all_equal(xs):
    return all(x == xs[0] for x in xs)


MAX_SURFACE_EXTENT = 16384


ZCULL_SUBREGION_ALLOCATION_FORMAT_Z_16X16X2_4X4 = 0x00000000
ZCULL_SUBREGION_ALLOCATION_FORMAT_ZS_16X16_4X4 = 0x00000001
ZCULL_SUBREGION_ALLOCATION_FORMAT_Z_16X16_4X2 = 0x00000002
ZCULL_SUBREGION_ALLOCATION_FORMAT_Z_16X16_2X4 = 0x00000003
ZCULL_SUBREGION_ALLOCATION_FORMAT_Z_16X8_4X4 = 0x00000004
ZCULL_SUBREGION_ALLOCATION_FORMAT_Z_8X8_4X2 = 0x00000005
ZCULL_SUBREGION_ALLOCATION_FORMAT_Z_8X8_2X4 = 0x00000006
ZCULL_SUBREGION_ALLOCATION_FORMAT_Z_16X16_4X8 = 0x00000007
ZCULL_SUBREGION_ALLOCATION_FORMAT_Z_4X8_2X2 = 0x00000008
ZCULL_SUBREGION_ALLOCATION_FORMAT_ZS_16X8_4X2 = 0x00000009
ZCULL_SUBREGION_ALLOCATION_FORMAT_ZS_16X8_2X4 = 0x0000000A
ZCULL_SUBREGION_ALLOCATION_FORMAT_ZS_8X8_2X2 = 0x0000000B
ZCULL_SUBREGION_ALLOCATION_FORMAT_Z_4X8_1X1 = 0x0000000C
ZCULL_SUBREGION_ALLOCATION_FORMAT_NONE = 0x0000000F


ZCULL_SUBREGIONS_ALGORITHM_STATIC = 0x00000000
ZCULL_SUBREGIONS_ALGORITHM_ADAPTIVE = 0x00000001


widthAlignPixels = 224
heightAlignPixels = 32
pixelSquaresByAliquots = 3584
aliquotTotal = 5120
zcullRegionByteMultiplier = 128
zcullRegionHeaderSize = 224
zcullSubregionHeaderSize = 1344
subregionCount = 16
subregionWidthAlignPixels = 224
subregionHeightAlignPixels = 64


def heuristic(width, height):
    info = ZcullInfo()
    info.width = width
    info.height = height

    width = next_multiple_of(width, subregionWidthAlignPixels)
    height = next_multiple_of(height, subregionHeightAlignPixels)

    info.normalized_aliquots = next_multiple_of(
        div_ceil(width * height, pixelSquaresByAliquots), subregionCount
    )
    normalized_aliquots_per_region = info.normalized_aliquots // subregionCount

    subregion_aliquots = [0] * subregionCount
    subregion_format = [ZCULL_SUBREGION_ALLOCATION_FORMAT_NONE] * subregionCount

    format_priorities = [
        (ZCULL_SUBREGION_ALLOCATION_FORMAT_Z_4X8_2X2, 8),
        (ZCULL_SUBREGION_ALLOCATION_FORMAT_Z_8X8_4X2, 4),
        (ZCULL_SUBREGION_ALLOCATION_FORMAT_Z_16X16_4X2, 2),
        (ZCULL_SUBREGION_ALLOCATION_FORMAT_Z_16X16X2_4X4, 1),
    ]

    remaining_aliquots = aliquotTotal
    for i in reversed(range(subregionCount)):
        for fmt, factor in format_priorities:
            aliquot_count = div_ceil(factor * normalized_aliquots_per_region, 2)
            if (i + 1) * aliquot_count <= remaining_aliquots:
                subregion_aliquots[i] = aliquot_count
                subregion_format[i] = fmt
                remaining_aliquots -= subregion_aliquots[i]
                break

    info.subregion_aliquots = [subregion_aliquots] * subregionCount

    info.region_format = "Z_4X4"
    info.subregion_aliquots = subregion_aliquots
    info.subregion_format = subregion_format
    info.aliquot_count = sum(info.subregion_aliquots)
    info.subregion_algorithm = ZCULL_SUBREGIONS_ALGORITHM_STATIC

    assert info.aliquot_count <= aliquotTotal

    return info


@settings(deadline=None)
@given(
    width=integers(1, MAX_SURFACE_EXTENT),
    height=integers(1, MAX_SURFACE_EXTENT),
)
def test_blob_properties(width, height):
    w1 = width
    h1 = height

    w2 = next_multiple_of(width, subregionWidthAlignPixels)
    h2 = next_multiple_of(height, subregionHeightAlignPixels)

    w3 = (h2 // subregionHeightAlignPixels) * subregionWidthAlignPixels
    h3 = (w2 // subregionWidthAlignPixels) * subregionHeightAlignPixels

    assert w2 * h2 == w3 * h3

    print(w2, h2)
    print(w3, h3)

    blob_info1 = generate_info_cached((w1, h1))
    blob_info2 = generate_info_cached((w2, h2))
    blob_info2.width = w1
    blob_info2.height = h1
    assert blob_info1 == blob_info2
    if w3 <= MAX_SURFACE_EXTENT and h3 <= MAX_SURFACE_EXTENT:
        blob_info3 = generate_info_cached((w3, h3))
        blob_info3.width = w1
        blob_info3.height = h1
        assert blob_info2 == blob_info3
    assert blob_info1.aliquot_count == sum(blob_info1.subregion_aliquots)
    if blob_info1.subregion_algorithm == ZCULL_SUBREGIONS_ALGORITHM_STATIC:
        assert all_equal(blob_info1.subregion_aliquots)
        assert all_equal(blob_info1.subregion_format)
    else:
        assert not all_equal(blob_info1.subregion_aliquots)
        assert not all_equal(blob_info1.subregion_format)


@settings(deadline=None)
@given(
    width=integers(1, MAX_SURFACE_EXTENT // subregionWidthAlignPixels),
    height=integers(1, MAX_SURFACE_EXTENT // subregionHeightAlignPixels),
)
def test_heuristic_matches_blob(width, height):
    width = width * subregionWidthAlignPixels
    height = height * subregionHeightAlignPixels
    heuristic_info = heuristic(width, height)
    blob_info = generate_info_cached((width, height))
    # assert blob_info.aliquot_count >= heuristic_info.aliquot_count
    if blob_info.subregion_algorithm == ZCULL_SUBREGIONS_ALGORITHM_STATIC:
        assert heuristic_info == blob_info
